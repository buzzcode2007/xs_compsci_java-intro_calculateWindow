# WindowBuilder Assignment
**Objective:** Study and test the implementations of the following Java topics using your preferred Java Compiler (usage of your Eclipse IDE is recommended):
1. WindowBuilder [Swing] Components
2. `getText()`
3. `setText()`
4. `parseInt()`

The following is a simple adding program that I’ve created for your reference, you are free to test it but try to improve from this example. Do provide screenshots of your work and submit it via the Activity Submission portal on Schoology.

1. In the Design tab, created a Frame with one TextField and two Buttons. In this example, my TextField variable in named **txtScreen**, while my teo Buttons are named **btnAdd** and **btnEquals**. You can change them in the Properties panel when you click them.

2. In the Source tab, created a Class variable (inside the class, but outside any method) **num** that shall hold the number that we input in the TextField. Also created an **`add()`** function with one parameter **`int x`**. The lines of code inside this will compute for the sum of our inputs as they are stored in num.

3. Scroll to our **btnAdd** Button object source code. We are able to retrieve any value from **txtScreen** using **`txtScreen.getText()`**. Now, we can insert that value into our **`add()`** function inside the **`actionPerformed()`** of our **btnAdd** object. Since **`add()`** can only accept integers as inputs, we use **`Integer.parseInt()`** to convert our input into a String before inserting it to **`add()`**. We also want to clear our TextField every time we click on the ‘+’ button using **`txtScreen.setText("")`**.

4. Scroll to our **btnEquals** Button object source code. Notice that the action for this button also contains an **`add()`** function. The purpose is to take the latest input and also compute it before displaying the sum. The output can be shown using the code at line #74. The **`setText()`** feature only handles Strings, so we can either convert it into String or do this little trick using concatenation. Finally, we clear the contents of **num** so it doesn’t overflow its value to further addition.

