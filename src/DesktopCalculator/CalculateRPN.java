package DesktopCalculator;

// Import modules. 
import java.util.*;
import java.util.regex.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;

// Create the window. 
public class CalculateRPN extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField GUI_TEXTFIELD_ENTRY_2;
	private JTextField GUI_TEXTFIELD_ENTRY_1;
	private JLabel GUI_LABEL_STATUS;
	private JComboBox GUI_COMBOBOX_OPERATION;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CalculateRPN WINDOW_FRAME = new CalculateRPN();
					WINDOW_FRAME.setVisible(true);
				} catch (Exception PROGRAM_PROBLEM) {
					PROGRAM_PROBLEM.printStackTrace();
				} 
			}
		});
	};
	
	public static double calculate(ArrayList<Object> ENTRIES_LIST) {
		/** Calculate in an RPN-like format. 
		 *  Parameters: 
		 *   ENTRIES_LIST (ArrayList<Object>): Way to solve
		 *	Returns: (double) result */
		
		// Calculate length. 
		int ENTRIES_LIST_LENGTH = ENTRIES_LIST.size();
		int OPERATION_ITERATIONS = 0;
		String OPERATION = "";
		double OPERATION_RESULT = 0;

		Queue<Double> NUMBERS_CURRENT = new ArrayDeque<Double>();

		// TO DO: fix entries search
		if (ENTRIES_LIST_LENGTH > 1) {
			for (int ENTRY_CURRENT = 0; ENTRY_CURRENT < ENTRIES_LIST_LENGTH; ENTRY_CURRENT++) {
				if (String.valueOf(ENTRIES_LIST.get(ENTRY_CURRENT)).matches("[-+\\/*×÷]")) {
					OPERATION = String.valueOf(ENTRIES_LIST.get(ENTRY_CURRENT));
					if (OPERATION_ITERATIONS <= 0) {OPERATION_RESULT = NUMBERS_CURRENT.poll();};

					switch (OPERATION) {
						case "*":
						case "×":
							do {
								OPERATION_RESULT *= NUMBERS_CURRENT.poll();
							} while (!NUMBERS_CURRENT.isEmpty());
							break;
						case "/":
						case "÷": 
							Double NUMBER_CURRENT = NUMBERS_CURRENT.poll();
							OPERATION_RESULT /= NUMBER_CURRENT;
							break;
						case "+": 
							do {
								OPERATION_RESULT += NUMBERS_CURRENT.poll();
							} while (!NUMBERS_CURRENT.isEmpty());
							break;
						case "-": 
							do {
								OPERATION_RESULT -= NUMBERS_CURRENT.poll();
							} while (!NUMBERS_CURRENT.isEmpty());
							break;
						default:
							break;
					};

					OPERATION_ITERATIONS++;	
				} else {
					NUMBERS_CURRENT.add((double) ENTRIES_LIST.get(ENTRY_CURRENT));
				};
			};
		};

		return (OPERATION_RESULT);
	};
	
	
	public CalculateRPN() {
		// Configure defaults. 
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		int GUI_WINDOW_HEIGHT = 175;
		int GUI_WINDOW_WIDTH = 250;
		
		// Window size
		setSize(new Dimension(GUI_WINDOW_WIDTH, GUI_WINDOW_HEIGHT)); // window size
		
		// Configure operations. 
		final String[] OPERATIONS_SUPPORTED = {"+", "-", "×", "÷"};
		
		// Configure math. 
		Pattern COMMANDENTRY_NUMBER = Pattern.compile("-?\\d+(\\.\\d+)?", Pattern.CASE_INSENSITIVE);
		
		// Configure appearance. 
		final Font FONT_MATH = new Font("Courier New", Font.PLAIN, 15);
		final Font FONT_MATH_RESULT = new Font("Courier New", Font.BOLD, 20);
		
		// Configure the layout. 
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{450, 0, 0};
		gridBagLayout.rowHeights = new int[]{26, 29, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);
		
		// Textboxes
		GUI_TEXTFIELD_ENTRY_1 = new JTextField();
		GUI_TEXTFIELD_ENTRY_1.setFont(FONT_MATH);
		GridBagConstraints gbc_GUI_TEXTFIELD_ENTRY_1 = new GridBagConstraints();
		gbc_GUI_TEXTFIELD_ENTRY_1.gridwidth = 2;
		gbc_GUI_TEXTFIELD_ENTRY_1.insets = new Insets(0, 0, 5, 0);
		gbc_GUI_TEXTFIELD_ENTRY_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_GUI_TEXTFIELD_ENTRY_1.gridx = 0;
		gbc_GUI_TEXTFIELD_ENTRY_1.gridy = 0;
		getContentPane().add(GUI_TEXTFIELD_ENTRY_1, gbc_GUI_TEXTFIELD_ENTRY_1);
		GUI_TEXTFIELD_ENTRY_1.setColumns(10);
		
		GUI_TEXTFIELD_ENTRY_2 = new JTextField();
		GUI_TEXTFIELD_ENTRY_2.setFont(FONT_MATH);
		GridBagConstraints gbc_GUI_TEXTFIELD_ENTRY_2 = new GridBagConstraints();
		gbc_GUI_TEXTFIELD_ENTRY_2.gridwidth = 2;
		gbc_GUI_TEXTFIELD_ENTRY_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_GUI_TEXTFIELD_ENTRY_2.insets = new Insets(0, 0, 5, 0);
		gbc_GUI_TEXTFIELD_ENTRY_2.gridx = 0;
		gbc_GUI_TEXTFIELD_ENTRY_2.gridy = 1;
		getContentPane().add(GUI_TEXTFIELD_ENTRY_2, gbc_GUI_TEXTFIELD_ENTRY_2);
		GUI_TEXTFIELD_ENTRY_2.setColumns(10);
		
		
		// Combo box
		GUI_COMBOBOX_OPERATION = new JComboBox(OPERATIONS_SUPPORTED);
		GUI_COMBOBOX_OPERATION.setFont(FONT_MATH);
		GridBagConstraints gbc_GUI_COMBOBOX_OPERATION = new GridBagConstraints();
		gbc_GUI_COMBOBOX_OPERATION.insets = new Insets(0, 0, 5, 5);
		gbc_GUI_COMBOBOX_OPERATION.fill = GridBagConstraints.HORIZONTAL;
		gbc_GUI_COMBOBOX_OPERATION.gridx = 0;
		gbc_GUI_COMBOBOX_OPERATION.gridy = 2;
		getContentPane().add(GUI_COMBOBOX_OPERATION, gbc_GUI_COMBOBOX_OPERATION);
		
		// Buttons
		JButton GUI_BUTTON_SOLVE = new JButton("Solve");
		GridBagConstraints gbc_GUI_BUTTON_SOLVE = new GridBagConstraints();
		gbc_GUI_BUTTON_SOLVE.insets = new Insets(0, 0, 5, 0);
		gbc_GUI_BUTTON_SOLVE.gridx = 1;
		gbc_GUI_BUTTON_SOLVE.gridy = 2;
		getContentPane().add(GUI_BUTTON_SOLVE, gbc_GUI_BUTTON_SOLVE);
		GUI_BUTTON_SOLVE.setVisible(false); // should be hidden by default
		
		JButton GUI_BUTTON_CLEAR = new JButton("⌫");
		GridBagConstraints gbc_GUI_BUTTON_CLEAR = new GridBagConstraints();
		gbc_GUI_BUTTON_CLEAR.gridx = 1;
		gbc_GUI_BUTTON_CLEAR.gridy = 3;
		getContentPane().add(GUI_BUTTON_CLEAR, gbc_GUI_BUTTON_CLEAR);
		GUI_BUTTON_CLEAR.setVisible(false); // should be hidden by default
		
		// Labels
		GUI_LABEL_STATUS = new JLabel("");
		GridBagConstraints gbc_GUI_LABEL_STATUS = new GridBagConstraints();
		gbc_GUI_LABEL_STATUS.insets = new Insets(0, 0, 0, 5);
		gbc_GUI_LABEL_STATUS.gridx = 0;
		gbc_GUI_LABEL_STATUS.gridy = 3;
		getContentPane().add(GUI_LABEL_STATUS, gbc_GUI_LABEL_STATUS);
		GUI_LABEL_STATUS.setVisible(false); // should be hidden by default
		
		
		// Events
		DocumentListener INTERFACE_BUTTONS_ACTIVESTATE_CHECKER = new DocumentListener() {
			public void insertUpdate(DocumentEvent e) {
				interface_update_edit ();
			}
			public void removeUpdate(DocumentEvent e) {
				interface_update_edit ();
			}
			public void changedUpdate(DocumentEvent e) {
				interface_update_edit ();
				
			}
			public void interface_update_edit() {
				boolean INTERFACE_TEXTFIELD_ENTRY_1_FILLED = COMMANDENTRY_NUMBER.matcher(GUI_TEXTFIELD_ENTRY_1.getText()).find();
				boolean INTERFACE_TEXTFIELD_ENTRY_2_FILLED = COMMANDENTRY_NUMBER.matcher(GUI_TEXTFIELD_ENTRY_2.getText()).find();
				
				GUI_BUTTON_SOLVE.setVisible((INTERFACE_TEXTFIELD_ENTRY_1_FILLED && INTERFACE_TEXTFIELD_ENTRY_2_FILLED));
				GUI_BUTTON_CLEAR.setVisible((INTERFACE_TEXTFIELD_ENTRY_1_FILLED || INTERFACE_TEXTFIELD_ENTRY_2_FILLED));
				GUI_TEXTFIELD_ENTRY_1.setFont(FONT_MATH);
			}
		};
		
		ActionListener INTERFACE_TEXTBOX_FOCUSSWITCH = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean INTERFACE_TEXTFIELD_ENTRY_1_FILLED = COMMANDENTRY_NUMBER.matcher(GUI_TEXTFIELD_ENTRY_1.getText()).find();
				boolean INTERFACE_TEXTFIELD_ENTRY_2_FILLED = COMMANDENTRY_NUMBER.matcher(GUI_TEXTFIELD_ENTRY_2.getText()).find();
				
				if (INTERFACE_TEXTFIELD_ENTRY_1_FILLED && INTERFACE_TEXTFIELD_ENTRY_2_FILLED) {
					GUI_BUTTON_SOLVE.requestFocusInWindow();
				} else if (INTERFACE_TEXTFIELD_ENTRY_1_FILLED) {
					GUI_TEXTFIELD_ENTRY_2.requestFocusInWindow();
				} else if (INTERFACE_TEXTFIELD_ENTRY_2_FILLED) {
					GUI_TEXTFIELD_ENTRY_1.requestFocusInWindow();
				};
			};
		};
		
		ActionListener ACTION_SOLVE = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean INTERFACE_BUTTONS_ACTIVESTATE = (COMMANDENTRY_NUMBER.matcher(GUI_TEXTFIELD_ENTRY_1.getText()).find() && COMMANDENTRY_NUMBER.matcher(GUI_TEXTFIELD_ENTRY_2.getText()).find());
				if (INTERFACE_BUTTONS_ACTIVESTATE) {
					ArrayList<Object> USER_INPUTS = new ArrayList<Object>(Arrays.asList(Double.parseDouble(GUI_TEXTFIELD_ENTRY_1.getText()), Double.parseDouble(GUI_TEXTFIELD_ENTRY_2.getText()), String.valueOf(GUI_COMBOBOX_OPERATION.getSelectedItem())));
					
					// Reset the error status. 
					GUI_LABEL_STATUS.setVisible(false);
					
					try {
						Double NUMBER_RESULT = calculate(USER_INPUTS);
						
						// Update the interface with the result. 
						GUI_TEXTFIELD_ENTRY_1.setText(String.valueOf(NUMBER_RESULT));
						GUI_TEXTFIELD_ENTRY_1.setFont(FONT_MATH_RESULT);
						GUI_TEXTFIELD_ENTRY_2.setText("");
					} catch(Exception CALCULATION_ERROR) {
						GUI_LABEL_STATUS.setText("Error: " + CALCULATION_ERROR);
						GUI_LABEL_STATUS.setVisible(true);
					};
				};
			};
		};
		
		ActionListener ACTION_CLEAR = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean INTERFACE_TEXTFIELD_ENTRY_1_FILLED = COMMANDENTRY_NUMBER.matcher(GUI_TEXTFIELD_ENTRY_1.getText()).find();
				boolean INTERFACE_TEXTFIELD_ENTRY_2_FILLED = COMMANDENTRY_NUMBER.matcher(GUI_TEXTFIELD_ENTRY_2.getText()).find();
				
				if (INTERFACE_TEXTFIELD_ENTRY_1_FILLED || INTERFACE_TEXTFIELD_ENTRY_2_FILLED) {
					GUI_TEXTFIELD_ENTRY_1.setText("");
					GUI_TEXTFIELD_ENTRY_2.setText("");
				};
			};
		};
		
		GUI_TEXTFIELD_ENTRY_2.addActionListener(INTERFACE_TEXTBOX_FOCUSSWITCH);
		GUI_TEXTFIELD_ENTRY_1.addActionListener(INTERFACE_TEXTBOX_FOCUSSWITCH);
		GUI_TEXTFIELD_ENTRY_2.getDocument().addDocumentListener(INTERFACE_BUTTONS_ACTIVESTATE_CHECKER);
		GUI_TEXTFIELD_ENTRY_1.getDocument().addDocumentListener(INTERFACE_BUTTONS_ACTIVESTATE_CHECKER);
		GUI_BUTTON_SOLVE.addActionListener(ACTION_SOLVE);
		GUI_BUTTON_CLEAR.addActionListener(ACTION_CLEAR);
	}
}